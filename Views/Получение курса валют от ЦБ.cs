using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Lab3Framework.DAL;
using Lab3Framework.BLL;

namespace Lab3Framework
{
    /// <summary>
    /// Основная форма приложения
    /// </summary>
    public partial class MainForm : Form
    {
        private RichTextBox richTextBoxOutData;
        private ComboBox comboBox1;
        private Button button1;
        private TextBox textBox1;
        private RichTextBox richTextBox1;
        private List<Currency> ActualCurranceList;
        private Button button2;
        private List<UserPredict> PredictList = new List<UserPredict>();
        private RichTextBox richTextBox2;
        private CurrencyLogic _CurrencyLogic = new CurrencyLogic();
 
        /// <summary>
        /// Основная форма приложения
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            ViewrichTextBoxOutData();
            _CurrencyLogic.AllCurancy += UpdateCurrency;
            _CurrencyLogic.AllPredictCurancy += UpdateCurrencyPredict;
            _CurrencyLogic.PredictResultCurancy += UpdateBigDeltaCurrency;
            _CurrencyLogic.RefreshCurrancy();
        }


        void UpdateCurrency(string message) => richTextBoxOutData.Text = message;
        void UpdateCurrencyPredict(string message) => richTextBox1.Text = message;
        void UpdateBigDeltaCurrency(string message) => richTextBox2.Text = message;


        /// <summary>
        /// Отображение данных
        /// </summary>
        private void ViewrichTextBoxOutData()
        {
            var CurrancesList = _CurrencyLogic.GetAllCurrancy();
            foreach (var CurrancyName in CurrancesList)
            {
                
                comboBox1.Items.Add(CurrancyName);
            }
            comboBox1.SelectedIndex = 0;
        }
 
        

        private void InitializeComponent()
        {
            this.richTextBoxOutData = new System.Windows.Forms.RichTextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // richTextBoxOutData
            // 
            this.richTextBoxOutData.Location = new System.Drawing.Point(12, 52);
            this.richTextBoxOutData.Name = "richTextBoxOutData";
            this.richTextBoxOutData.Size = new System.Drawing.Size(607, 173);
            this.richTextBoxOutData.TabIndex = 0;
            this.richTextBoxOutData.Text = "";
            this.richTextBoxOutData.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(244, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(408, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(291, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(13, 232);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(337, 74);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(544, 11);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Refresh";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(376, 231);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(243, 74);
            this.richTextBox2.TabIndex = 6;
            this.richTextBox2.Text = "";
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(631, 318);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.richTextBoxOutData);
            this.Name = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, System.EventArgs e)
        {

        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            _CurrencyLogic.AddPredict((string)comboBox1.SelectedItem, float.Parse(textBox1.Text));
            _CurrencyLogic.RefreshCurrancy();
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            _CurrencyLogic.RefreshCurrancy();
        }
    }
}