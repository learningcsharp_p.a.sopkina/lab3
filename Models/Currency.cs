﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3Framework
{
    public class Currency
    {
        public string NumberCode { get; set; }
        public string LetterCode { get; set; }
        public string CurrencyUnit { get; set; }
        public string CurrencyName { get; set; }
        public float Rate { get; set; }


    }
}
