﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3Framework
{
    public class UserPredict
    {
        public string PredictCurrencyName { get; }
        public float PredictRate { get; }

        public UserPredict(string predictCurrencyName, float predictRate)
        {
            PredictCurrencyName = predictCurrencyName;
            PredictRate = predictRate;
        }
    }
}
