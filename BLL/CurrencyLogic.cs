﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3Framework.DAL;

namespace Lab3Framework.BLL
{
    public class CurrencyLogic
    {
        public delegate void AllCurrancyStrHandler(string message);
        public event AllCurrancyStrHandler AllCurancy;
        public delegate void PredictCurrancyStrHandler(string message);
        public event PredictCurrancyStrHandler AllPredictCurancy;
        public delegate void PredictResultCurrancyStrHandler(string message);
        public event PredictCurrancyStrHandler PredictResultCurancy;
        CentralBank _centralBank = new CentralBank();

        public List<string> GetAllCurrancy()
        {
            List<string> result = new List<string>();
            List<Currency> ActualCurranceList = _centralBank.GetExchangeRates();

            foreach (var it in ActualCurranceList)
            {
                result.Add(it.CurrencyName);
            }
            return result;
        }

        public void RefreshCurrancy()
        {
            List<Currency> ActualCurranceList = _centralBank.GetExchangeRates();

            string CurrencyRates = "";
            string PredictCurrencyRates = "";
            string BigDeltaPredictCurrencyRates = "";
            foreach (var ActualCurrance in ActualCurranceList)
            {
                CurrencyRates += $"{ActualCurrance.CurrencyName} : {ActualCurrance.Rate,2}\n";
                
                foreach(var Predict in CurrencyPredicts.PredictList)
                {
                    if(ActualCurrance.CurrencyName == Predict.PredictCurrencyName)
                    {
                        float deltaPredict = ActualCurrance.Rate / Predict.PredictRate * 100;
                        PredictCurrencyRates += $"{ActualCurrance.CurrencyName} : {Predict.PredictRate} | {ActualCurrance.Rate} || {deltaPredict}\n";
                        if(deltaPredict>=115 || deltaPredict<=75)
                        {
                            BigDeltaPredictCurrencyRates += $"{ActualCurrance.CurrencyName} : {deltaPredict}\n";
                        }
                        break;
                    }
                }
            }
            try { AllCurancy(CurrencyRates); } catch { }
            try { AllPredictCurancy(PredictCurrencyRates); } catch { }
            try { PredictResultCurancy(BigDeltaPredictCurrencyRates); } catch { }

        }

        public void AddPredict(string nameCurrency, float predictRate)
        {
            CurrencyPredicts.PredictList.Add(new UserPredict(nameCurrency, predictRate));
        }

    }
}
