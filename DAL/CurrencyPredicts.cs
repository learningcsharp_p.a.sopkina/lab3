﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3Framework.DAL
{
    public static class CurrencyPredicts
    {
        public static List<UserPredict> PredictList { get; set; }
        static CurrencyPredicts()
        {
            PredictList = new List<UserPredict>();
        }
    }
}
