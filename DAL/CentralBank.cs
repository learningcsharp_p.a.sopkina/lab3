﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Lab3Framework.DAL
{
    public class CentralBank
    {
        /// <summary>
        /// Адрес стартовой страници
        /// </summary>
        private const string StartPageLink = @"https://www.cbr.ru/currency_base/daily/";

        /// <summary>
        /// Курсы валют
        /// </summary>
        /// <returns></returns>
        public List<Currency> GetExchangeRates()
        {
            var res = new List<string>();
            List<Currency> result = new List<Currency>();
            try 
            {
                var htmlDoc = new HtmlWeb().Load(StartPageLink);

                var rows = htmlDoc.DocumentNode.SelectNodes("//table[@class='data']//tr");
                bool firstStr = true;
                foreach (var cell in rows)
                {
                    var r = cell.InnerText.Replace("  ", "");
                    var c = Regex.Replace(r, @"\r\n", "|").Split('|');
                    if (firstStr) firstStr = false;
                    else result.Add(new Currency
                    {
                        NumberCode = c[1],
                        LetterCode = c[2],
                        CurrencyUnit = c[3],
                        CurrencyName = c[4],
                        Rate = float.Parse(c[5])
                    });
                    res.Add(r);
                }
            }
            catch
            {
                
            }
            
            return result;
        }

    }




}
